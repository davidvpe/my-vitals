// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { checkRequest } from '../../../utils/middleware'
import Weight from '../../../models/weight'
import dbConnect from '../../../utils/db'

export default async function handler(req, res) {
    if (req.method === 'GET' && checkRequest(req)) {
        await dbConnect()
        const weight = await Weight.findOne({}).sort({ date: -1 })

        let latestUpdate = weight ? weight.date : new Date('01/10/2019')
        latestUpdate = new Date(latestUpdate.valueOf() + 3600)
        res.status(200).json({
            latest: latestUpdate.toLocaleString('en-US', {
                dateStyle: 'full',
                timeStyle: 'full',
            }),
        })
    } else {
        res.status(404).send()
    }
}
