import Head from 'next/head'
import Image from 'next/image'
import { Col, Container, Row, Tooltip } from 'react-bootstrap'
import {
    Area,
    AreaChart,
    Brush,
    CartesianGrid,
    Line,
    LineChart,
    XAxis,
    YAxis,
} from 'recharts'
import styles from '../styles/Home.module.css'
import dbConnect from '../utils/db'
import Weight from '../models/weight'
import { useEffect, useRef, useState } from 'react'

export async function getServerSideProps() {
    await dbConnect()
    const weights = await Weight.find({}).sort({ date: 1 }).lean()
    const last = weights[weights.length - 1]
    return {
        props: {
            weights: weights.map((w) => ({
                date: w.date.toLocaleDateString('en-US', {
                    dateStyle: 'medium',
                }),
                weight: w.value,
            })),
            latestUpdate: last.date.toLocaleDateString('en-US', {
                dateStyle: 'long',
            }),
        },
    }
}

export default function Home({ weights, latestUpdate }) {
    const father = useRef(null)
    const [chart, setChart] = useState(null)

    useEffect(() => {
        console.log('what')
        setChart(initializeChart(father.current.clientWidth))
    }, [father])

    const initializeChart = (width) => {
        return (
            <LineChart
                width={width}
                height={600}
                data={weights}
                margin={{
                    top: 40,
                    right: 40,
                    bottom: 20,
                    left: 20,
                }}
            >
                <CartesianGrid vertical={false} />
                <XAxis dataKey='date' label='Date' />
                <YAxis domain={['auto', 'auto']} label='Weight' />
                <Tooltip
                    wrapperStyle={{
                        borderColor: 'white',
                        boxShadow: '2px 2px 3px 0px rgb(204, 204, 204)',
                    }}
                    contentStyle={{
                        backgroundColor: 'rgba(255, 255, 255, 0.8)',
                    }}
                    labelStyle={{
                        fontWeight: 'bold',
                        color: '#FFFFFF',
                    }}
                />
                <Line dataKey='weight' stroke='#ff7300' dot={false} />
                <Brush dataKey='date' startIndex={weights.length - 30}>
                    <AreaChart>
                        <CartesianGrid />
                        <YAxis hide domain={['auto', 'auto']} />
                        <Area
                            dataKey='weight'
                            stroke='#ff7300'
                            fill='#ff7300'
                            dot={false}
                        />
                    </AreaChart>
                </Brush>
            </LineChart>
        )
    }

    const presentation = [
        `I've been overweight for a really long time, I think the first time I noticed was when I was 10 y/o. At some point I just stopped caring and decided to live my life the way I wanted. This was of course, a not very smart choice.`,
        `So in November 2019 I got my self a smart scale and stepped on it almost every day, started the classic diet, you know, protein and salads, kill the fats! And it worked!, for a while, then I went back to my hometown to spend the holidays and boom, when I was back I recovered most of my weight.`,
        `I created this webpage as some sort of "blog" but is not really a blog, or maybe it will be, who knows...ANYWAY...for now I will put here some graphs of my progress losing weight and also to improve my web coding skills`,
        `so... without further ado, here they are!`,
    ]
    return (
        <Container fluid className='bg-dark text-white'>
            <Head>
                <title>My Vitals by David Velarde</title>
                <meta name='description' content='NextJS App for my vitals' />
            </Head>
            <Container className='p-5'>
                <Row>
                    <Col xs={12} xl={6}>
                        <h1>My Vitals</h1>
                        <h3 className='mt-5'>Welcome!</h3>
                        {presentation.map((line, i) => (
                            <p className='mt-3' key={`line-${i}`}>
                                {line}
                            </p>
                        ))}
                        <p>Latest update was at: {latestUpdate}</p>
                    </Col>
                </Row>
                <Row ref={father}>{chart}</Row>
            </Container>
        </Container>
    )
}
