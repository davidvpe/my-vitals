export const checkRequest = (req) => {
    const {
        headers: { sec },
    } = req

    if (sec && sec === 'DAVID!VELARDE') {
        return true
    } else {
        return false
    }
}
