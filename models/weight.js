import mongoose from 'mongoose'

const structure = {
    date: {
        type: Date,
        unique: true,
    },
    value: Number,
}

const WeightSchema = new mongoose.Schema(structure, { timestamps: true })

module.exports = mongoose.models.Weight || mongoose.model('Weight', WeightSchema)
