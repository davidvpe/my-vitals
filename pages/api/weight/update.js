import dbConnect from '../../../utils/db'
import Weight from '../../../models/weight'
import { checkRequest } from '../../../utils/middleware'
// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default async function handler(req, res) {
    if (req.method === 'POST' && checkRequest(req)) {
        const records = JSON.parse(req.body.values)

        await dbConnect()
        await Weight.insertMany(
            records.map((w) => {
                return {
                    value: w.weight,
                    date: w.date,
                }
            })
        ).catch((err) => {
            console.log(err)
        })
        res.status(200).json({ r: true })
        return
    } else {
        res.status(404).send()
        return
    }
}
